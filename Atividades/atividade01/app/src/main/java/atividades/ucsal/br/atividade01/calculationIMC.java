package atividades.ucsal.br.atividade01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class calculationIMC extends AppCompatActivity {

    private EditText heightET;
    private EditText weightET;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculation_imc);
        heightET = findViewById(R.id.inputHeight);
        weightET = findViewById(R.id.inputWeight);
        result = findViewById(R.id.result);
    }

    public void btnCalculate(View view) {
        Double height = Double.parseDouble(heightET.getText().toString());
        Double weight = Double.parseDouble(weightET.getText().toString());
        DecimalFormat df = new DecimalFormat("#0.00");
        String imc = "Seu imc é de: " + df.format(calculateIMC(height, weight));
        String res = imc + stateIMC(calculateIMC(height, weight));
        result.setText(res);
    }

    private double calculateIMC(Double height, Double weight) {
        return weight / (height * height);
    }

    private String stateIMC(Double imc) {
        String state = " Estado: ";

        if (imc < 18.5) {
            state += "Magreza";
        } else if (18.5 < imc && imc < 24.9) {
            state += "Normal";
        } else if (25.0 < imc && imc < 29.9) {
            state += "Sobrepeso";
        }else if (30.0 < imc && imc < 39.9) {
            state += "Obesidade";
        }else if (imc < 40.0) {
            state += "Obesidade Grave";
        }

        return state;
    }

}
