package com.example.aula05;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText userEditText;
    private EditText passwordEditText;
    private TextView error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userEditText = findViewById(R.id.user);
        passwordEditText = findViewById(R.id.password);
        error = findViewById(R.id.error);
    }

    public void login(String user, String password) throws Exception {
        validate(user, password);
    }

    public void btnRedirect(View view) {
        String user = userEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        try {
            login(user, password);
            Intent intent = new Intent(this, Main2Activity.class);
            startActivity(intent);
        } catch (Exception e) {
            error.setText(e.getMessage());
            error.setVisibility(View.VISIBLE);
        }
    }

    private void validate(String user, String password) throws Exception {
        if(user.equalsIgnoreCase("") || password.equalsIgnoreCase("")) {
            throw new Exception("Campos obrigatórios");
        } else if (!user.equalsIgnoreCase("Andrei") || !password.equals("12345")) {
            throw new Exception("Usuário e senha inválidos");
        }
    }
}
